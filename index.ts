// import Address from "./src/address";
// import { config } from "./src/config";
import * as readline from "readline";
import Address from "./src/address";
import { config } from "./src/config";

function run() {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    rl.on("close", function () {
        console.log("\nBYE BYE !!!");
        process.exit(0);
    });

    const ask = function () {
        const signature = `
        #################################################################
        #                                                               #
        #      #     ###  ###         #     #       # ##### #       #   #
        #     # #    #  # #  #       # #    ##     ##   #   ##     ##   #
        #    #####   ###  ###       #####   # #   # #   #   # #   # #   #
        #   #     #  #    #        #     #  #  # #  #   #   #  # #  #   #
        #  #       # #    #       #       # #   #   # ##### #   #   #   #
        #                                                               #
        #################################################################`

        const addresses = [
            "No 13 Jalan 5/28, Bandar Rinching,43500 Semenyih,Selangor",
            "lot 2547, jalan delima merah, kg. chabang empat, 16210 tumpat kelantan",
            "No : 10E-2 Lot 90516 Kg Kuala Sungai Baru Batu 13, Jalan Klang 47100 Puchong Selangor",
            "28-02-01 Block 28, Pangsapuri Seroja, Jalan Arca U8/80, Seksyen U8, Bukit Jelutong 40150 Shah Alam, Selangor",
            "No 11, Chendering, 21080 Kuala Terengganu, Terengganu",
            "No 11, Kuala Terengganu, Chendering",
        ]

        console.log(signature)
        console.log("\nExample Address :")
        addresses.map((address) => {
            console.log(address)
        })
        rl.question('\nPlease input and address that you would like to tokenize or copy any of the example above \n(input "EXIT" to end this application) \n\ninput: ', function (answer) {
            if (answer === "exit" || answer === "EXIT") {
                return rl.close();
            } else {
                console.log("Output : ")
                console.log(new Address(answer, config).tokenizeAddress())
                rl.question('\ncontinue or exit ? \(input "YES" to continue or input "EXIT" to end this application) \n\ninput: ', function (answerTwo) {
                    if (answerTwo === "yes" || answerTwo === "YES" || answerTwo === "Y" || answerTwo === "y") {
                        ask();
                    } else if (answerTwo === "exit" || answerTwo === "EXIT") {
                        return rl.close();
                    } else {
                        console.log("Unexpected answer, please input a correct one as suggested, lets consider to continue this time...")
                        ask();
                    }
                })
            }
        });
    };
    ask();
}
run();