import { /* assert, */ expect } from 'chai';
import Address from './address';
import { config } from './config';

/**
 * @description Address
 */
describe("Address - setter , getter & whole processes", function () {
    it("setter , getter & whole processes", function () {
        const addresses = [
            "No 13 Jalan 5/28, Bandar Rinching,43500 Semenyih,Selangor",
            "lot 2547, jalan delima merah, kg. chabang empat, 16210 tumpat kelantan",
            "No : 10E-2 Lot 90516 Kg Kuala Sungai Baru 9ae3 9E-1 Batu 13, Jalan Klang 47100 Puchong Selangor 28-02-01 Terengganu Pahang Kelantan Melaka",
            "No 11, Chendering, 21080 Kuala Terengganu, Terengganu",
            "No 11, Kuala Terengganu, Chendering, Petaling Jaya Puchong Subang Jaya",
            "28-02-01 Block 28, Pangsapuri Seroja, Jalan Arca U8/80, Seksyen U8, Bukit Jelutong 40150 Shah Alam, Selangor"
        ]
        console.log("Testing addresses :")
        console.log(addresses)
        const res = addresses.map((address) => {
            const addressObj = new Address(address, config);
            return addressObj.tokenizeAddress()
        })

        const expectedResult = [{
            apt: 'No 13',
            postcode: '43500',
            city: '',
            state: 'selangor',
            section: 'Jalan 5/28, Bandar Rinching, Semenyih,'
        },
        {
            apt: 'lot 2547',
            postcode: '16210',
            city: 'tumpat',
            state: 'kelantan',
            section: 'jalan delima merah, kg chabang empat,'
        },
        {
            apt: 'No : 10E-2, Lot 90516, 9E-1, 28-02-01',
            postcode: '47100',
            city: 'puchong',
            state: 'selangor, terengganu, pahang, kelantan, melaka',
            section: 'Kg Kuala Sungai Baru 9ae3, Batu 13, Jalan Klang'
        },
        {
            apt: 'No 11',
            postcode: '21080',
            city: 'kuala terengganu',
            state: 'terengganu',
            section: 'Chendering,'
        },
        {
            apt: 'No 11',
            postcode: '',
            city: 'kuala terengganu, petaling jaya, puchong, subang jaya',
            state: '',
            section: 'Chendering,'
        },
        {
            apt: '28-02-01',
            postcode: '40150',
            city: '',
            state: 'selangor',
            section:
                'Block 28, Pangsapuri Seroja, Jalan Arca U8/80, Seksyen U8, Bukit Jelutong Shah Alam,'
        }]

        console.log("Expected Results :")
        console.log(expectedResult)
        console.log("Current Results :")
        console.log(res)
        
        expect(res).to.be.an('array').to.deep.equal(expectedResult);
    })
})