import { AddressConfig } from "./config";
import StringProcessor from "./string-processor";

export default class Address extends StringProcessor {

    private aptIdentifierMixWith: string[] = [];
    private aptSeparator: string[] = [];
    private aptIdentifier: string[] = [];
    private cityIdentifier: string[] = [];
    private stateIdentifier: string[] = [];
    private sectionIdentifier: string[] = [];
    private postcodeRange: { minRange: number, maxRange: number };

    private apt: string; // 'No 11',
    private section: string; // 'Chendering',
    private postcode: string; // '21080',
    private city: string; // 'Kuala Terengganu',
    private state: string; // 'Terengganu'

    private tokenizedRaw: string[] = [];

    constructor(str: string, options: AddressConfig) {
        super(str)
        const {
            apiIdentifierMixWith, aptIdentifier, aptSeparator,
            cities, sectionIdentifier, states, postcodeIdentifierRange
        } = options;
        this._setAptIdentifierMixWith(apiIdentifierMixWith)
        this._setAptSeparator(aptSeparator)
        this._setAptIdentifier(aptIdentifier)
        this._setSectionIdentifier(sectionIdentifier);
        this._setCitiesIdentifier(cities);
        this._setStatesIdentifier(states);
        this._setPostcodeRange(postcodeIdentifierRange)
        this._setAddress();
    }

    /**
     * @description run tokenization and set each token to its own prop
     */
    private _setAddress() {
        this.tokenizedRaw = super.cleanEachTokenizedItem(super.tokenizeBySpacesAndCommas(), [" "]);
        this._setApt()
        this._setPostcode()
        this._setCity()
        this._setState()
        this._setSection();
    }

    /**
     * @description get the raw address and tokenized address
     * @returns { apt: string, postcode: string, city: string, state: string, section: string }
     */
    public tokenizeAddress(): { apt: string, postcode: string, city: string, state: string, section: string } {
        return {
            apt: this.apt,
            postcode: this.postcode,
            city: this.city,
            state: this.state,
            section: this.section,
        };
    }

    /**
     * @description set identifiers for section to be found as a token
     * @example kg chabang tiga, where "kg" is the identifier
     * @param {string[]} newIdentifiers identifiers for section
     */
    private _setSectionIdentifier(newIdentifiers: string[]): void {
        this.sectionIdentifier = super.arrayOfStringToLowerCases(newIdentifiers);
    }

    /**
     * @description set identifiers for possible separator for apt
     * @example No : 9E-1, where ":" is the separator
     * @param {string[]} newIdentifiers identifiers for apt separator
     */
    private _setAptSeparator(newIdentifiers: string[]): void {
        this.aptSeparator = super.arrayOfStringToLowerCases(newIdentifiers);
    }

    /**
     * @description set identifiers for apt to be found as a token
     * @example No 10E-1, where "No" is the identifier
     * @param {string[]} newIdentifiers identifiers for apt
     */
    private _setAptIdentifier(newIdentifiers: string[]): void {
        this.aptIdentifier = super.arrayOfStringToLowerCases(newIdentifiers);
    }

    /**
     * @description set identifiers for apt that might be mixed with
     * @example 28-2-1, where "-" is mixed with number for apt
     * @param {string[]} newIdentifiers identifiers for section
     */
    private _setAptIdentifierMixWith(newIdentifiers: string[]): void {
        this.aptIdentifierMixWith = super.arrayOfStringToLowerCases(newIdentifiers);
    }

    /**
     * @description set identifiers for cities to be found as a token
     * @example "Kajang", "Bangi" & "Kuala Lumpur", where all of these are the identifiers for cities that will be searched in the address
     * @param {string[]} newIdentifiers list of identifiers for cities
     */
    private _setCitiesIdentifier(newIdentifiers: string[]): void {
        this.cityIdentifier = super.arrayOfStringToLowerCases(newIdentifiers);
    }

    /**
     * @description set identifiers for states to be found as a token
     * @example "Kelantan", "Melaka" & "Pulau Pinang", where all of these are the identifiers for states that will be searched in the address
     * @param {string[]} newIdentifiers list of identifiers for states
     */
    private _setStatesIdentifier(newIdentifiers: string[]): void {
        this.stateIdentifier = super.arrayOfStringToLowerCases(newIdentifiers);
    }

    /**
     * @description set minRange and maxRange as identifier for postcode to be found as a token
     * @example minRange : 1000, maxRange : 98859, where "1000" is the minimum range and "98859" is the maximum range of the identifier for postcode that will be searched in the address
     * @param {number} minRange identifiers for minimum range for postcode
     * @param {number} maxRange identifiers for maximum range for postcode
     */
    private _setPostcodeRange(range: { minRange: number, maxRange: number }) {
        this.postcodeRange = range;
    }

    /**
     * @todo Minimum requirement : token that Starts with “No “ followed by a series of numbers
     * @description responsible for finding and setting apts from stored raw address.
     * will be affected by {aptIdentifierMixWith} , {aptIdentifier} , {aptSeparator} & {stateIdentifier}
     */
    private _setApt(): void {
        const tokens = [...super.cleanEachTokenizedItem(this.tokenizedRaw, [","])]
        const newApts: string[] = [];
        let skipIndex = -1;
        let takenTokens = 0;
        tokens.map((item, index) => {
            if (index > skipIndex) {
                const isIncludeWord: boolean = this.aptIdentifier.includes(item.toLowerCase());
                const hasNumber: boolean = !this.stateIdentifier.includes(this.tokenizedRaw[index - 1]) && (new StringProcessor(item).hasNumber({ mixSpecific: this.aptIdentifierMixWith }) || new StringProcessor(item).hasNumber({ mixAlphabet: true }));
                if (isIncludeWord) {
                    if (this.aptSeparator.includes(tokens[index + 1]) && tokens[index + 2] ? new StringProcessor(tokens[index + 2]).hasNumber() : false) {
                        skipIndex = index + 2 // wordNo + separator + aptnumber
                        newApts.push(this.tokenizedRaw.splice(index - takenTokens, 3).join(" "))
                        takenTokens += 3
                    } else if (tokens[index + 1] ? new StringProcessor(tokens[index + 1]).hasNumber() : false) {
                        skipIndex = index + 1 // wordNo + aptnumber
                        newApts.push(this.tokenizedRaw.splice(index - takenTokens, 2).join(" "))
                        takenTokens += 2
                    }
                }
                if (hasNumber) {
                    newApts.push(this.tokenizedRaw.splice(index - takenTokens, 1).join(""))
                    skipIndex = index;
                    takenTokens += 1;
                }
            }
        })
        this.apt = super.cleanEachTokenizedItem(newApts, [","]).join(", ")
    }

    /**
     * @todo Minimum requirement : Any other text un-parsable as other components is considered as {section}
     * @description responsible for finding and setting sections from stored raw address.
     * will be affected by {sectionIdentifier}
     * @requires {} needed to be last to get the rest of the token left by other setters!
     */
    private _setSection(): void {
        let newSection: string = "";
        let beforeHasTrailingComma: boolean = false;
        this.tokenizedRaw.map((token, index) => {
            const isPreword: boolean = index > 0 && this.sectionIdentifier.includes(token.toLocaleLowerCase());
            newSection = `${newSection}${isPreword && !beforeHasTrailingComma ? "," : ""}${index > 0 ? " " : ""}${token}`;
            beforeHasTrailingComma = token[token.length - 1] === ",";
        })
        this.section = newSection
        this.tokenizedRaw = [];
    }


    /**
     * @todo Minimum requirement : Numbers from range of 01000-98859
     * @description responsible for finding and setting postcode from stored raw address.
     * will be affected by {postcodeRange} & {aptIdentifier}
     */
    private _setPostcode(): void {
        const newPostcodes: string[] = [];
        this.tokenizedRaw.map((item, index) => {
            const noPreset = this.tokenizedRaw[index - 1] !== undefined ? !this.aptIdentifier.includes(this.tokenizedRaw[index - 1].toLowerCase()) : true;
            const cleanedItem = new StringProcessor(item).cleanTrailingComma();
            if (parseInt(cleanedItem) >= this.postcodeRange.minRange && parseInt(cleanedItem) <= this.postcodeRange.maxRange && noPreset) {
                newPostcodes.push(cleanedItem)
                this.tokenizedRaw.splice(index, 1)
            }
        })
        this.postcode = newPostcodes.join(", ");
    }

    /**
     * @todo Minimum requirement : Matches any of the cities provided
     * @description responsible for finding and setting cities from stored raw address.
     * will be affected by {cityIdentifier}
     */
    private _setCity(): void {
        const results: string[] = [];
        this.cityIdentifier.map((city) => {
            const tokenizedCity = new StringProcessor(city).tokenizeBySpaces();
            this.tokenizedRaw.map((_token, index) => {
                const holder = [...this.tokenizedRaw];
                const subSetTokens: string = holder.splice(index, tokenizedCity.length).join(" ")
                const subSetTokensCleaned = new StringProcessor(subSetTokens).cleanTrailingComma();
                if (city === subSetTokensCleaned.toLowerCase()) {
                    results.push(city);
                    this.tokenizedRaw.splice(index, tokenizedCity.length);
                }
            })
        })
        this.city = results.join(", ");
    }

    /**
     * @todo Minimum requirement : Matches any of the states provided
     * @todo can be re-factored (cities & state setter) into re-usable function from String Processor
     * @description responsible for finding and setting states from stored raw address.
     * will be affected by {stateIdentifier}
     */
    private _setState(): void {
        const results: string[] = [];
        this.stateIdentifier.map((state) => {
            const tokenizedStates = new StringProcessor(state).tokenizeBySpaces();
            this.tokenizedRaw.map((_token, index) => {
                const holder = [...this.tokenizedRaw];
                const subSetTokens: string = holder.splice(index, tokenizedStates.length).join(" ")
                const subSetTokensCleaned = new StringProcessor(subSetTokens).cleanTrailingComma();
                if (state === subSetTokensCleaned.toLowerCase()) {
                    results.push(state);
                    this.tokenizedRaw.splice(index, tokenizedStates.length);
                }
            })
        })
        this.state = results.join(", ");
    }
}