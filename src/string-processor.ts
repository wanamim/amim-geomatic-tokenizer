
/**
 * @property {boolean} mixAlphabet include single alphabet
 * @property {string[]} mixSpecific include custom single char
 */
interface OptionsHasNumbetr {
    mixAlphabet?: boolean,
    mixSpecific?: string[],
}

export default class StringProcessor {
    private str: string

    constructor(str: string) {
        this._strSetter(str);
    }

    /**
     * @description set the str prop for this class
     * @param {string} newStr the string to be processed
     * @throws Input need to be type string
     */
    private _strSetter(newStr: string): void {
        if (typeof newStr !== "string") {
            throw new Error("Input need to be type string");
        } else {
            this.str = newStr;
        }
    }

    /**
     * @description get the value of prop str in this class
     * @returns {sring} value of prop str
     */
    public getStr(): string {
        return this.str;
    }

    /**
     * @description to split one single string into an array of string/character by defined splitter.
     * @param {string[]} arrayCharOrStr list of characters or string to determine where to split the string
     * @param {boolean} clean default : true, to remove token of undefined and empty string
     * @returns array of string
     */
    public tokenizeBy(arrayCharOrStr: string[], clean: boolean = true): string[] {
        // /(.*?、)/g
        const joined = "(.*?[" + arrayCharOrStr.join("") + "])"; // regexp 'character class' with front wildcards(dot)
        const regexp = new RegExp(joined, "g")
        const splited = this.str.split(regexp);
        return clean ? this.cleanArrayOfString(splited) : splited;
    }

    /**
     * @description to split one single string into an array of string/character by white space.
     * @returns tokenized by empty space
     * @see {tokenizeBy}
     */
    public tokenizeBySpaces(): string[] {
        return this.tokenizeBy([" "]);
    }

    /**
     * @description to split one single string into an array of string/character by white space or comma ",".
     * @returns tokenized by empty space
     * @see {tokenizeBy}
     */
    public tokenizeBySpacesAndCommas(): string[] {
        return this.tokenizeBy([",", " "]);
    }

    /**
     * @description to clean out all un-needed values in a string
     * @param {string[]} arrayStr array of string to be cleaned
     * @param {string[]} itemToRemove default : ["", undefined, " "], array of character or string to be removed
     * @returns array of string
     */
    public cleanArrayOfString(arrayStr: string[], itemToRemove: string[] = ["", undefined, " "]): string[] {
        const cleaned = arrayStr.filter((item) => {
            const clear: boolean = !itemToRemove.includes(item);
            return clear;
        })
        return cleaned;
    }

    /**
     * @description remove trailing comma of a string
     * @returns string of removed trainling comma
     */
    public cleanTrailingComma(): string {
        return this.str[this.str.length - 1] === "," ? this.str.slice(0, -1) : this.str
    }

    /**
     * @description clean each token of an array of string from defined items
     * @param {string[]} items array of string to be cleans one by one
     * @param {string[]} toRemove array of what to be removed
     * @returns list of cleaned string
     */
    public cleanEachTokenizedItem(items: string[], toRemove: string[]): string[] {
        const regexp = new RegExp("[." + toRemove.join("") + "]", "g")
        const newItem = items.map((item: string) => {
            return item.replace(regexp, '')
        })
        return newItem;
    }

    /**
     * @description convert all array of string into lower cases
     * @param {string[]} arrStr items to be converted
     * @returns lisf of string
     */
    public arrayOfStringToLowerCases(arrStr: string[]): string[] {
        return arrStr.map((item) => {
            return item.toLowerCase();
        })
    }

    /**
     * @description check if a string is a number
     * @returns boolean
     */
    public stringIsANumber(): boolean {
        return !isNaN(parseInt(this.str));
    }

    /**
     * @description to check if a string is a number
     * @param {OptionsHasNumbetr} options options to customize result
     * @returns boolean
     */
    public hasNumber(options?: OptionsHasNumbetr): boolean {
        const holder = options ? options : {}
        const {
            mixAlphabet = false,
            mixSpecific = [],
        } = holder
        const hasNumber: boolean = /\d/.test(this.str);
        // const hasAnyOtherChar: boolean = /[^0-9]/.test(this.str)
        const hasAlphabet: boolean = /[0-9][a-zA-Z]($|[^a-zA-Z])/.test(this.str)
        const hasSpecificChar: boolean = mixSpecific.length > 0 ? new RegExp(`[.${mixSpecific.join("")}]`).test(this.str) : true;
        return mixAlphabet ? hasAlphabet && hasNumber && hasSpecificChar : hasNumber && hasSpecificChar;
    }
}