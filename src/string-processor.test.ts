import { /* assert,  */expect } from 'chai';
import StringProcessor from './string-processor';

/**
 * @description StringProcessor - test setter getter
 */
describe('StringProcessor', function () {
    it("setter and getter", function () {
        const testStr = "this is a test case string"
        const strProc = new StringProcessor(testStr);
        expect(strProc.getStr()).to.equal(testStr)
    })
    it('error wrong param type for constructor', function () {
        try {
            //@ts-ignore
            new StringProcessor(0)
        } catch (err) {
            expect(err.message.includes("Input need to be type string")).to.equal(true);
        }
    })
})

/**
 * @description StringProcessor - test tokenizeBy
 */
describe('StringProcessor - tokenizeBy', function () {
    it("tokenizeBy comma", function () {
        const testStr = "this, is a, test case, string"
        const strProc = new StringProcessor(testStr);
        const res = strProc.tokenizeBy([","]);
        expect(res).to.be.an('array').to.deep.equal([ 'this,', ' is a,', ' test case,', ' string' ])
    })
    it("tokenizeBy space", function () {
        const testStr = "this, is a, test case, string"
        const strProc = new StringProcessor(testStr);
        const res = strProc.tokenizeBy([" "]);
        expect(res).to.be.an('array').to.deep.equal([ 'this, ', 'is ', 'a, ', 'test ', 'case, ', 'string' ])
    })
})

/**
 * @description StringProcessor - test tokenizeBySpaces
 */
 describe('StringProcessor - tokenizeBySpaces', function () {
    it("tokenizeBySpaces", function () {
        const testStr = "this, is a, test case, string"
        const strProc = new StringProcessor(testStr);
        const res = strProc.tokenizeBySpaces();
        expect(res).to.be.an('array').to.deep.equal([ 'this, ', 'is ', 'a, ', 'test ', 'case, ', 'string' ])
    })
})

/**
 * @description StringProcessor - test tokenizeBySpacesAndCommas
 */
 describe('StringProcessor - tokenizeBySpacesAndCommas', function () {
    it("tokenizeBySpacesAndCommas", function () {
        const testStr = "this, is a, test case, string"
        const strProc = new StringProcessor(testStr);
        const res = strProc.tokenizeBySpacesAndCommas();
        expect(res).to.be.an('array').to.deep.equal([ 'this,', 'is ', 'a,', 'test ', 'case,', 'string' ])
    })
})

/**
 * @description StringProcessor - test cleanArrayOfString
 */
 describe('StringProcessor - cleanArrayOfString', function () {
    it("cleanArrayOfString and default values", function () {
        const testStr = [ undefined, 'is ', '', ' ', 'case,', 'string' ]
        const res = new StringProcessor("").cleanArrayOfString(testStr);
        expect(res).to.be.an('array').to.deep.equal([ 'is ', 'case,', 'string' ])
    })
})

/**
 * @description StringProcessor - test cleanTrailingComma
 */
 describe('StringProcessor - cleanTrailingComma', function () {
    it("cleanTrailingComma", function () {
        const testStr = 'case,'
        const res = new StringProcessor(testStr).cleanTrailingComma();
        expect(res).to.equal('case')
    })
})

/**
 * @description StringProcessor - test cleanEachTokenizedItem
 */
 describe('StringProcessor - cleanEachTokenizedItem', function () {
    it("cleanEachTokenizedItem", function () {
        const tokenized = [ 'this, ', 'is ', 'a, ', 'test ', 'case, ', 'string' ]
        const res = new StringProcessor("").cleanEachTokenizedItem(tokenized, [","," "]);
        expect(res).to.be.an('array').to.deep.equal([ 'this', 'is', 'a', 'test', 'case', 'string' ]);
    })
})

/**
 * @description StringProcessor - test arrayOfStringToLowerCases
 */
 describe('StringProcessor - arrayOfStringToLowerCases', function () {
    it("arrayOfStringToLowerCases", function () {
        const tokenized = [ 'tHis, ', 'IS ', 'a, ', 'teST ', 'case, ', 'stRINg' ]
        const res = new StringProcessor("").arrayOfStringToLowerCases(tokenized);
        expect(res).to.be.an('array').to.deep.equal([ 'this, ', 'is ', 'a, ', 'test ', 'case, ', 'string' ]);
    })
})

/**
 * @description StringProcessor - test stringIsANumber
 */
 describe('StringProcessor - stringIsANumber', function () {
    it("stringIsANumber true", function () {
        const test = "910293"
        const res = new StringProcessor(test).stringIsANumber();
        expect(res).to.equal(true)
        const test2 = "019093"
        const res2 = new StringProcessor(test2).stringIsANumber();
        expect(res2).to.equal(true)
    })
    it("stringIsANumber false", function () {
        const test3 = "something"
        const res3 = new StringProcessor(test3).stringIsANumber();
        expect(res3).to.equal(false)
    })
})

/**
 * @description StringProcessor - test hasNumber
 */
 describe('StringProcessor - hasNumber', function () {
    it("hasNumber true", function () {
        const test = "9E"
        const res = new StringProcessor(test).hasNumber({mixAlphabet: true});
        expect(res).to.equal(true)
        const test2 = "9A9-"
        const res2 = new StringProcessor(test2).hasNumber({mixAlphabet: true, mixSpecific:["-"]});
        expect(res2).to.equal(true)
    })
    it("hasNumber false", function () {
        const test3 = "999"
        const res3 = new StringProcessor(test3).hasNumber({mixAlphabet: true, mixSpecific:["-"]});
        expect(res3).to.equal(false)
        const test4 = "-A-"
        const res4 = new StringProcessor(test4).hasNumber({mixAlphabet: true, mixSpecific:["-"]});
        expect(res4).to.equal(false)
    })
})