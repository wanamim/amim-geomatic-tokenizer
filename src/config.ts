/**
 * @description list of cities : Kuala Terengganu, Kuala Lumpur, Kajang, Bangi, Damansara, Petaling Jaya, 
 * Puchong, Subang Jaya, Cyberjaya, Putrajaya, Mantin, Kuching, Seremban,
 */
export const CITIES: string[] = [
    "Kuala Terengganu",
    "Kuala Lumpur",
    "Kajang",
    "Bangi",
    "Damansara",
    "Petaling Jaya",
    "Puchong",
    "Subang Jaya",
    "Cyberjaya",
    "Putrajaya",
    "Mantin",
    "Kuching",
    "Seremban",
]

/**
 * @description list of states consist of : Selangor, Terengganu, Pahang, Kelantan, Melaka, 
 * Pulau Pinang, Kedah, Johor, Perlis, Sabah, Sarawak
 */
export const STATES: string[] = [
    "Selangor",
    "Terengganu",
    "Pahang",
    "Kelantan",
    "Melaka",
    "Pulau Pinang",
    "Kedah",
    "Johor",
    "Perlis",
    "Sabah",
    "Sarawak",
]

export interface AddressConfig {
    postcodeIdentifierRange: {
        minRange: number,
        maxRange: number,
    },
    cities: string[],
    states: string[],
    sectionIdentifier: string[],
    apiIdentifierMixWith: string[],
    aptSeparator: string[],
    aptIdentifier: string[],
}

export const config: AddressConfig = {
    postcodeIdentifierRange: {
        minRange: 1000,
        maxRange: 98859,
    },
    aptIdentifier: ["no", "lot", "no."],
    aptSeparator: [":"],
    apiIdentifierMixWith: ["-"],
    cities: [...CITIES, "tumpat"],
    states: [...STATES],
    sectionIdentifier: [
        "jalan",
        "kg",
        "kampung",
        "jln",
        "batu",
        "seksyen",
        "Block",
        "blok"
    ],
}