## Introduction
An application developed using TypeScript

## Run:
1) clone this repository by running this in cmd prompt or git bash : git clone https://gitlab.com/wanamim/amim-geomatic-tokenizer amim-address
2) change directory to the folder "amim-address" by running : cd amim-address
3) install ts-node globally : npm install ts-node -g
4) run this to install : npm install
5) then run this to start the application : npm run start

## Unit Test:
run "npm run test" in the main directory

## Configuration:
can be found in ./src/config.ts along with its type interface.
